#include "csv_models.hpp"
#include "line_models.hpp"
#include "Config.h"
#include <string>
#include <vector>
#include <sstream>

#ifndef __PREARRANGE_H__
#define __PREARRANGE_H__

using namespace std;

class PreArrange{
private:
	vector<CsvModel> R1;
	vector<CsvModel> R2;
	vector<CsvModel> R3;
	string DEL_NO;
	vector<string> split(const string &s, char delim);
	void fill_R( vector<CsvModel>& R, vector<string>& R_elems )
	{
		for(int i=0; i<R_elems.size(); i++ )
		{	
			string csv_name = R_elems[i] + ".csv";
			R.push_back( CsvModel(csv_name) );
		} 
	}

public:
	PreArrange( const char* file_ini );
	vector<CsvModel> get_R1() const { return R1; }
	vector<CsvModel> get_R2() const { return R2; }
	vector<CsvModel> get_R3() const { return R3; }
	string get_DEL_NO() const { return DEL_NO; }
};

PreArrange::PreArrange( const char* file_ini )
{
	Config R_settings("files.txt");
	string R1_str = R_settings.Read("R1", R1_str );
	string R2_str = R_settings.Read("R2", R2_str );
	string R3_str = R_settings.Read("R3", R3_str );
	DEL_NO = R_settings.Read("DEL_Number", DEL_NO); 
	
	vector<string> R1_elems = split( R1_str, ',' );
	vector<string> R2_elems = split( R2_str, ',' );
	vector<string> R3_elems = split( R3_str, ',' );
	
	fill_R( R1, R1_elems );
	fill_R( R2, R2_elems );
	fill_R( R3, R3_elems );	 
}

vector<string> PreArrange::split(const string& s, char delim)
{
	vector<string> elems;
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) 
	{
		elems.push_back(item);
	}
	return elems;
}

#endif 
