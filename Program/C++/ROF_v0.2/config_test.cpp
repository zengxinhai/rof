#include <fstream>
#include <iostream>
#include <string>
#include "csv_parser.hpp"
using namespace std;
//This function parses the rules and index file to double array of 5 elements.
void parse_attach( string Filename, double* attach )
{
	csv_parser csv( Filename.c_str() );
	for( int i=0; i<5; i++ )
		attach[i] = atof( csv.get_value( 1, i+1 ).c_str() );
}

int main()  
{  
    double rules[5];
    parse_attach( "72_rules.txt", rules );
    for( int i=0; i<5; i++ )
    	cout << rules[i] << endl;     
    return 0;  
}  
