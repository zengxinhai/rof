#include <iostream>
#include <fstream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>

using namespace std;
int main()
{
	
	ofstream files;
	files.open( "files.txt" );
	string file_name;
	cout << "R1: ";
	cin >> file_name;
	files << "R1=" << file_name << endl;
	
	cout << "R2: ";
	cin >> file_name;
	files << "R2=" << file_name << endl;
	
	cout << "R3: ";
	cin >> file_name;
	files << "R3=" << file_name << endl;
	
	cout << "DEL Number: ";
	cin >> file_name;
	files << "DEL_Number=" << file_name << endl;
	
	
	files.close();
	mkdir( "Result", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
	return 0;
}
