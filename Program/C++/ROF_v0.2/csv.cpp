#include "csv_parser.hpp"
#include "line_models.hpp"
#include "csv_models.hpp"
#include "Config.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <vector>
#include <string>
#include <time.h>
#include "preArrange.hpp"
#include "ROF.hpp"
#include "AttachTracker.hpp"
#include "arrange.hpp"

using namespace std;

void to_csv( vector<LineModel> Result_lines, string title_line, string outpath )
{
	 ofstream out_file( outpath.c_str() );
	 out_file << title_line << endl;
	 
	 vector<string> np_lines;
	 for( int i=0; i<Result_lines.size(); ++i )
	 	np_lines.push_back( Result_lines[i].to_string() );
	 
	 set<string> s( np_lines.begin(), np_lines.end() );
	 np_lines.assign( s.begin(), s.end() );
	 
	 for( int i=0; i<np_lines.size(); ++i )
	 	out_file << np_lines[i] << endl;
	 out_file.close();
	 cout << outpath << ": " << np_lines.size() << endl;	 
}
		
int main(int argc, char* argv[])
{
	
	time_t t_start, t_end;
	
	PreArrange preA( "files.txt" );
	CsvModel AA = preA.get_AminoAcid(); 
	CsvModel CHO = preA.get_Aldehyde();  
	CsvModel COOH = preA.get_Acid(); 
	string DEL_NO = preA.get_DEL_NO();
	t_start = time(NULL);	
	//ROF rofCal( AA, CHO, COOH, rules, index );
	//vector<LineModel> Result_R1_lines = rofCal.get_result_R1();
	//vector<LineModel> Result_R2_lines = rofCal.get_result_R2();
	//vector<LineModel> Result_R3_lines = rofCal.get_result_R3();
	//Do the calculating
	Arrange arrange( AA, CHO, COOH, DEL_NO ); 
	
	vector<LineModel> Result_R1_lines = arrange.get_R1();
	vector<LineModel> Result_R2_lines = arrange.get_R2();
	vector<LineModel> Result_R3_lines = arrange.get_R3();
	t_end = time(NULL);
	
	//Output the result
	string title_line = AA.get_title_line();
	to_csv( Result_R1_lines, title_line, "Result/R1.csv");
	to_csv( Result_R2_lines, title_line, "Result/R2.csv");
	to_csv( Result_R3_lines, title_line, "Result/R3.csv");
	cout << "Time: " << difftime(t_end, t_start) << "s" << endl;		

	return 0;
}
