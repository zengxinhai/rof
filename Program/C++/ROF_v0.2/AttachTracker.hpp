#include <string>
#include "line_models.hpp"
#include "csv_parser.hpp"

#ifndef __NAMETRACKER_H__
#define __NAMETRACKER_H__
using namespace std;
class AttachTracker{
private:
	string R1;
	string R2;
	string R3;
	LineModel rules;
	LineModel index;
	void parse_attach( string Filename, double* attach )
	{
		csv_parser csv( Filename.c_str() );
		for( int i=0; i<5; i++ )
			attach[i] = atof( csv.get_value( 1, i+1 ).c_str() );
	}
public:
	AttachTracker(string R1, string R2, string R3, string DEL_NO);
	LineModel get_rules() const {return rules;}
	LineModel get_index() const {return index;}
};

AttachTracker::AttachTracker(string _R1, string _R2, string _R3, string DEL_NO)
{
	R1 = _R1;
	R2 = _R2;
	R3 = _R3;
	string rulesfile = DEL_NO + "_rules.txt";
	string indexfile = DEL_NO + "_index_" + R1 + "_" + R2 + "_" + R3 + ".txt";
	double rules_data[5];
	double index_data[5];
	parse_attach( rulesfile, rules_data );
	parse_attach( indexfile, index_data );
	rules = LineModel( rules_data );
	index = LineModel( index_data );
} 
#endif
