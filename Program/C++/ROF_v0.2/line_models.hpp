#include <vector>
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

#ifndef __LINE_MODEL_H__
#define __LINE_MODEL_H__

//methods: +, <, push_back, cout
class LineModel{
private: 
	string info;
	double data[5];
public:
	LineModel() { info = "None"; }
	LineModel( double* input )
	{
		for( int i= 0; i<5; i++)
			data[i] = input[i];
		info = "None";
	}
	LineModel( const double* input, const string _info )
	{
		for( int i= 0; i<5; i++)
			data[i] = input[i];
		info = _info;
	}
	~LineModel(){}
	LineModel operator+(const LineModel& T) const
	{
		LineModel data_sum;
		for(int i=0; i<5; i++)
			data_sum.data[i] = data[i] + T.data[i];
		return data_sum;
	}
	bool operator==( const LineModel& T ) const
	{
		return (info == T.info);
	}
	bool operator<(const LineModel& T) const
	{
		for( int i=0; i<5; i++ )
			if (data[i]>T.data[i])
				return false;
		return true;
	}
	const string get_info() const{ return info; }
		
	
	const string to_string() const
	{
		string con_string = "";
		con_string += info;		
		for( int i=0; i<5; i++ )
		{
			stringstream dss;
			dss << data[i];
			con_string += ( ','+dss.str() );
		}
		return con_string;
	}
		
	friend ostream& operator<<( ostream& os, const LineModel& d );
};

ostream& operator<<( ostream& os, const LineModel& d )
{
	os << "Info: " << d.info << endl;
	os << "Data: ";
	for( int i=0; i<5; i++ )
		os << d.data[i] << " ";
	return os;
}

#endif
