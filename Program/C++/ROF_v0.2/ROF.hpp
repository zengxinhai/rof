#include "line_models.hpp"
#include "csv_models.hpp"
#include <vector>
#include <string>
#include <set>

#ifndef __ROF_H__
#define __ROF_H__

class ROF{
private:
	CsvModel R1;
	CsvModel R2;
	CsvModel R3;
	LineModel rules;
	LineModel index;
	vector<LineModel> R1_result;
	vector<LineModel> R2_result;
	vector<LineModel> R3_result;
	void duplicate_idx( vector<int>& idx)
	{
		set<int> s( idx.begin(), idx.end() );
		idx.assign( s.begin(), s.end() );
	}
	void set_lines( vector<LineModel>& L, CsvModel& R, vector<int>& idx )
	{
		for( int i=0; i<idx.size(); i++ )
		{
			int line_no = idx[i];
			L.push_back( R[line_no] );
		}		
	}
public:
	ROF( CsvModel R1, CsvModel R2, CsvModel R3, LineModel rules, LineModel index );
	vector<LineModel> get_result_R1() const {return R1_result;}
	vector<LineModel> get_result_R2() const {return R2_result;}
	vector<LineModel> get_result_R3() const {return R3_result;}
};

ROF::ROF( CsvModel _R1, CsvModel _R2, CsvModel _R3, LineModel _rules, LineModel _index )
{
	R1 = _R1;
	R2 = _R2;
	R3 = _R3;
	rules = _rules;
	index = _index;
	
	
	vector<int> R1_result_idx;
	vector<int> R2_result_idx;
	vector<int> R3_result_idx;
	#pragma omp parallel for
	for( int i=0; i<R1.get_num_lines()-1; ++i )
	{
		for( int j=0; j<R2.get_num_lines()-1; ++j )
		{
			for(int k=0; k<R3.get_num_lines()-1; ++k)
			{
				if( (R1[i] + R2[j] + R3[k] + index) < rules ) 
				{
					R1_result_idx.push_back(i);
					R2_result_idx.push_back(j);
					R3_result_idx.push_back(k);
					
				}
			}
		}
	}
	
	duplicate_idx( R1_result_idx );
	duplicate_idx( R2_result_idx );
	duplicate_idx( R3_result_idx );
	
	set_lines( R1_result, R1, R1_result_idx );
	set_lines( R2_result, R2, R2_result_idx );
	set_lines( R3_result, R3, R3_result_idx );	
}



#endif
	
