#include "csv_parser.hpp"
#include "line_models.hpp"
#include <iostream>
#include <vector>
#include <string>

#ifndef __CSVMODEL_H__
#define __CSVMODEL_H__
using namespace std;

class CsvModel{
private:
	int num_lines;
	string name;
	string title_line;
	LineModel* plines;
	LineModel get_items( csv_parser& csv, int row_num );
	
public:
	CsvModel();
	CsvModel( string filePath );
	const LineModel operator[]( int idx ) const;
	//CsvModel operator+( const CsvModel& ) const; 
	const int get_num_lines() const { return num_lines; }
	const string get_title_line() const { return title_line; }
	//void push_back( const LineModel& line ){ lines.push_back(line); num_lines += 1; }
	void to_csv( string outpath ) const;
	string get_name() {return name;}
	~CsvModel(){ }
};

LineModel CsvModel::get_items( csv_parser& csv, int row_num )
{
	double data[5];
	string info = csv.get_value(row_num, 1);
	for( int i = 4; i < 9; i++ )	
		data[i-4] =  atof( csv.get_value(row_num, i).c_str() );
	for( int i = 2; i < 4; i++ )
		info += ( "," + csv.get_value(row_num, i) );
	return LineModel( data, info );
}

CsvModel::CsvModel()
{
	num_lines = 0;
	title_line = "";
	plines = NULL;
	name="";
}
	
	
CsvModel::CsvModel( string filePath )
{
	csv_parser csv(filePath);
	num_lines = csv.get_total_lines();
	title_line = csv.get_line( 1 );
	plines = new LineModel[num_lines-1];
	for( int i=0; i<num_lines-1; i++ )
		plines[i] = get_items( csv, i+2 );
	for( int i=0; i<filePath.size()-4; i++ )
		name += filePath[i];
}

const LineModel CsvModel::operator[](int idx) const
{
	return plines[idx];
}
/*
CsvModel CsvModel::operator+( const CsvModel& T ) const
{
	CsvModel sum;
	sum.lines = lines;
	for( int i=0; i<T.lines.size(); i++)
		sum.lines.push_back( T.lines[i] );
	sum.num_lines = num_lines + T.num_lines;
	sum.title_line = title_line;
	return sum;
}
*/

void CsvModel::to_csv( string outpath ) const
{
	ofstream out(outpath.c_str());
	out << title_line << endl;
	for( int i=0; i<num_lines-1; ++i )
		out << plines[i].to_string() << endl;
	out.close();
}
	
#endif	
	
	
