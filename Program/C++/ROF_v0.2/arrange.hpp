#include <vector>
#include <algorithm>
#include "csv_models.hpp"
#include "line_models.hpp"
#include "AttachTracker.hpp"
#include "ROF.hpp"

#ifndef __ARRANGE_H__
#define __ARRANGE_H__
class Arrange{
private:
	vector<CsvModel> R1;
	vector<CsvModel> R2;
	vector<CsvModel> R3;
	vector<LineModel> final_R1;
	vector<LineModel> final_R2;
	vector<LineModel> final_R3;
	void extend_vec( vector<LineModel>& target, vector<LineModel> source )
	{
		for( int i=0; i< source.size(); i++ )
			target.push_back( source[i] );
	}
	void Add_R_combi( CsvModel R1, CsvModel R2, CsvModel R3, string DEL_NO )
	{
		AttachTracker Tracker( R1.get_name(), R2.get_name(), R3.get_name(), DEL_NO );
		ROF rofCal( R1, R2, R3, Tracker.get_rules(), Tracker.get_index() );
		extend_vec( final_R1, rofCal.get_result_R1() );
		extend_vec( final_R2, rofCal.get_result_R2() );
		extend_vec( final_R3, rofCal.get_result_R3() );
	}

		
public:
	Arrange( vector<CsvModel> R1, vector<CsvModel> R2, vector<CsvModel> R3, string DEL_NO )
	{
		
		for( int i=0; i<R1.size(); ++i )
			for( int j=0; j<R2.size(); ++j )
				for( int k=0; k<R3.size(); ++k )
					Add_R_combi( R1[i], R2[j], R3[k], DEL_NO );				
	}
	
	vector<LineModel> get_R1() { return final_R1; }
	vector<LineModel> get_R2() { return final_R2; }
	vector<LineModel> get_R3() { return final_R3; }
	
};

#endif
	
		
